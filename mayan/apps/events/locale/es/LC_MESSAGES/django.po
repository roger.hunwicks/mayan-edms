# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Roberto Rosario, 2015
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-11-23 02:54-0400\n"
"PO-Revision-Date: 2016-03-21 21:02+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Spanish (http://www.transifex.com/rosarior/mayan-edms/language/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:19 links.py:27 links.py:31 permissions.py:7 views.py:38
msgid "Events"
msgstr "Eventos"

#: apps.py:56
msgid "Timestamp"
msgstr "Marca de tiempo"

#: apps.py:58
msgid "Actor"
msgstr "Actor"

#: apps.py:60
msgid "Verb"
msgstr "Verbo"

#: classes.py:17
#, python-brace-format
msgid "Unknown or obsolete event type: {0}"
msgstr "Tipo de evento desconocido u obsoleto: {0}"

#: models.py:13
msgid "Name"
msgstr "Nombre"

#: models.py:20
msgid "Event type"
msgstr "Tipo de evento"

#: models.py:21
msgid "Event types"
msgstr "Tipos de eventos"

#: permissions.py:9
msgid "Access the events of an object"
msgstr "Acceder a los eventos de un objeto"

#: views.py:31 views.py:90
msgid "Target"
msgstr "Objetivo"

#: views.py:75
#, python-format
msgid "Events for: %s"
msgstr "Eventos para: %s"

#: views.py:98
#, python-format
msgid "Events of type: %s"
msgstr "Eventos de tipo: %s"
