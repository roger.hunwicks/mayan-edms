|PyPI badge| |Build Status| |Coverage badge| |Documentation| |License badge| |Python version|

|Logo|

Description
-----------

Free Open Source Electronic Document Management System.

`Website`_

`Video demostration`_

`Documentation`_

`Translations`_

`Mailing list (via Google Groups)`_

|Animation|

License
-------

This project is open sourced under `Apache 2.0 License`_.

Installation
------------

To install Mayan EDMS, simply do:

.. code-block:: bash

    $ virtualenv venv
    $ source venv/bin/activate
    (venv) $ pip install mayan-edms
    (venv) $ mayan-edms.py initialsetup
    (venv) $ mayan-edms.py runserver

Point your browser to 127.0.0.1:8000 and use the automatically created admin
account.


.. _Website: http://www.mayan-edms.com
.. _Video demostration: http://bit.ly/pADNXv
.. _Documentation: http://readthedocs.org/docs/mayan/en/latest/
.. _Translations: https://www.transifex.com/projects/p/mayan-edms/
.. _Mailing list (via Google Groups): http://groups.google.com/group/mayan-edms
.. _Apache 2.0 License: https://www.apache.org/licenses/LICENSE-2.0.txt

.. |Build Status| image:: https://gitlab.com/mayan-edms/mayan-edms/badges/master/build.svg
   :target: https://gitlab.com/mayan-edms/mayan-edms/commits/master
.. |Logo| image:: https://gitlab.com/mayan-edms/mayan-edms/raw/master/docs/_static/mayan_logo.png
.. |Animation| image:: https://gitlab.com/mayan-edms/mayan-edms/raw/master/docs/_static/overview.gif
.. |PyPI badge| image:: http://img.shields.io/pypi/v/mayan-edms.svg?style=flat
   :target: http://badge.fury.io/py/mayan-edms
.. |License badge| image:: https://img.shields.io/pypi/l/mayan-edms.svg?style=flat
.. |Analytics| image:: https://ga-beacon.appspot.com/UA-52965619-2/mayan-edms/readme?pixel
.. |Coverage badge| image:: https://codecov.io/gitlab/mayan-edms/mayan-edms/coverage.svg?branch=master
   :target: https://codecov.io/gitlab/mayan-edms/mayan-edms?branch=master
.. |Documentation| image:: https://readthedocs.org/projects/mayan/badge/?version=latest
   :target: http://mayan.readthedocs.io/en/latest
.. |Python version| images:: https://img.shields.io/pypi/pyversions/mayan-edms.svg

|Analytics|
